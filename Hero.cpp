#include <SFML/Graphics.hpp>
#include <math.h>

#include "Config.h"
#include "Hero.h"

using namespace sf;

extern bool game_over;
extern bool win;
extern std::vector<std::string> s_map;

Hero::Hero()
{
    pos_x = MAP_SCALE + MAP_SCALE/2;
    pos_y = MAP_SCALE + MAP_SCALE/2;
    dx=dy=0;
    size_in_map = 1;
    circle.setRadius(size_in_map*MAPPED_HERO);
    circle.setFillColor (Color::Red);
    circle.setPosition (START_POS_X, START_POS_Y);
}

void Hero::restart()
{
    pos_x = MAP_SCALE + MAP_SCALE/2;;
    pos_y = MAP_SCALE + MAP_SCALE/2;;
}

void Hero::update (int& coin_count)
{
    dy+=GRAVITATION;
    int map_x = (pos_x+dx)/MAP_SCALE;
    int map_x_e = (pos_x+dx+(size_in_map*MAPPED_HERO*2))/MAP_SCALE;
    int map_y = (pos_y+dy)/MAP_SCALE;
    int map_y_e = (pos_y+dy+(size_in_map*MAPPED_HERO*2))/MAP_SCALE;
    int J = s_map[0].size();
    int I = s_map.size();

    int x = pos_x/MAP_SCALE;
    int y = pos_y/MAP_SCALE;
    int x_e = (pos_x+(size_in_map*MAPPED_HERO*2))/MAP_SCALE;
    int y_e = (pos_y+(size_in_map*MAPPED_HERO*2))/MAP_SCALE;

    if(map_x_e<J && map_y_e<I && map_x>=0 && map_y>=0)
    {
        if (s_map[y][x]=='4' && s_map[y_e][x] =='4' && s_map[y][x_e] =='4' && s_map[y_e][x_e] == '4')
            s_map[pos_y/MAP_SCALE][pos_x/MAP_SCALE] = '0';
        if (s_map[y][x]=='2' && s_map[y_e][x] =='2' && s_map[y][x_e] =='2' && s_map[y_e][x_e] == '2')
        {
            int distance = sqrt (pow (pos_x+(size_in_map*MAPPED_HERO) - ((((int)((pos_x+MAPPED_HERO)/MAP_SCALE))*MAP_SCALE)+MAP_SCALE/2), 2)
                              + pow (pos_y+(size_in_map*MAPPED_HERO) - ((((int)((pos_y+MAPPED_HERO)/MAP_SCALE))*MAP_SCALE)+MAP_SCALE/2), 2));
            if (distance < MAPPED_HERO+COIN_SIZE)
            {
                coin_count+=1;
                s_map[pos_y/MAP_SCALE][pos_x/MAP_SCALE] = '0';
            }
        }
        if (dx>0)  // right
        {
            if (s_map[y][map_x_e]!='1' && s_map[y_e][map_x_e]!='1')
                pos_x += dx;
        }
        if (dx<0)  // left
        {
            if (s_map[y][map_x]!='1' && s_map[y_e][map_x]!='1')
                pos_x += dx;
        }
        if (dy>0)  //down
        {
            if (s_map[map_y_e][x]!='1' && s_map[map_y_e][x_e]!='1')
                pos_y += dy;
        }
        if (dy<0)  // up
        {
            if (s_map[map_y][x]!='1' && s_map[map_y][x_e]!='1')
                pos_y += dy;
            else
                dy = 0;
        }

        if (s_map[map_y_e][x]!='1' && s_map[map_y_e][x_e]!='1')
            in_air=true;
        else
        {
            in_air = false;
            dy *= -0.7;
            if (fabs(dy)<0.0001)
                dy=0;
        }
    }
    else
        game_over = true;
    dx = 0;
    if (coin_count == 0 && s_map[pos_y/MAP_SCALE][pos_x/MAP_SCALE] == '3')
        win = true;

    circle.setPosition (pos_x, pos_y);
}
