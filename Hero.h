#ifndef HERO_H_INCLUDED
#define HERO_H_INCLUDED

#include <SFML/Graphics.hpp>

#include "Config.h"

using namespace sf;

extern std::vector<std::string> s_map;

class Hero
{
    float pos_x;
    float pos_y;
    int size_in_map;
public:
    bool in_air;
    float dx;
    float dy;
    CircleShape circle;

    Hero();
    void restart();
    void update(int& coin_count);
};

#endif // HERO_H_INCLUDED
