#ifndef CONFIG_H_INCLUDED
#define CONFIG_H_INCLUDED

//----------HERO------------
#define START_POS_X 200
#define START_POS_Y 200
#define MAPPED_HERO MAP_SCALE/10
#define SPEED_X 0.2
#define SPEED_Y 0.4

//--------MAP---------------
#define MAP_SCALE 50

//-------WORLD--------------
#define GRAVITATION 0.0015
#define COIN_SIZE 6

#endif // CONFIG_H_INCLUDED
