#include <iostream>
#include <SFML/Graphics.hpp>
#include <string>
#include <vector>

#include <sstream>

#include "Config.h"
#include "Hero.h"

using namespace sf;

int level_count = 0;
int coin_count=1;
bool game_over = false;
bool win = false;

/**
    1 - platform
    2 - coin
    3 - new level
    4 - shadow wall
    5 - fake coin
*/

/*
{
        "111111111111111111111",
        "100000000000000000001",
        "110000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "111111111111111111111"
    }

*/

std::vector<std::vector<std::string> > maps =
{
    {
        "111111111111111111111",
        "100000000000000000001",
        "110000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000200000000031",
        "111111111111111111111"
    },
    {
        "111111111111111111111",
        "100000000000000000001",
        "114444444444444444441",
        "100000000000000000001",
        "104400444004440444001",
        "104040404000400040001",
        "104040404000400040001",
        "104400444004440040001",
        "100000020000000000001",
        "100000020000000000001",
        "100000000000000000001",
        "100000000000000000441",
        "100000000000000005431",
        "111111111111111111111"
    },
    {
        "111111111111111111111",
        "100100000000000000001",
        "110101111111111111101",
        "110001210000000000001",
        "110001010111111111101",
        "110001010100002000101",
        "110001010100001000101",
        "110001010100000000101",
        "110001010100000000101",
        "110001010100000000101",
        "110001000100000000001",
        "110001110111111111101",
        "110204200000000000031",
        "115111110111111111101"
    },
    {
        "111111111111111111111",
        "100010020102000000031",
        "110010010101000000001",
        "100010000000000000001",
        "100010000000000000001",
        "100010000000000000001",
        "100010000000000000001",
        "100010000000000000001",
        "100010000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100020000100000000001",
        "100010000000000100001"
    },
    {
        "111111111111111111111",
        "104144444444444444241",
        "114141111111111111141",
        "144144444414444444441",
        "144144444412444444441",
        "144144444411111111441",
        "124144444444444441441",
        "114144444444444441441",
        "144144444444444441441",
        "144144444444444441441",
        "144144444444444441441",
        "144144444444444441441",
        "144444444444444441411",
        "145114444444444444431"
    },
    {
        "111111111111111111111",
        "100000000000000000131",
        "110000000000000000141",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "100000000000000000001",
        "125222222522222225221",
        "111111111111111111141"
    },
    {
        "133333333333333333331",
        "100000000000000000001",
        "110000000000000000001",
        "104444044440000000001",
        "104000040040000000001",
        "104004040040000000001",
        "104444044440000000001",
        "100000000000000000001",
        "104440404044404040001",
        "104040404040400400001",
        "104440444044400400001",
        "104040404040400400001",
        "100000200000000000001",
        "144444144444444444441"
    },
    {
        "111111111111111111111",
        "144444444444444444441",
        "114444444444444444441",
        "144444444444444444441",
        "143444443433343444341",
        "143444443443443344341",
        "143444443443443434341",
        "144343434443443443341",
        "144434344433343444341",
        "144444444444444444441",
        "144444444444444444441",
        "144444444444444444441",
        "144444444444444444441",
        "111111111111111111111"
    }
};

std::vector<std::string> s_map = maps[0];

int main()
{
    int level_amount = maps.size();
    int I = s_map.size();
    int J = s_map[0].size();
    RenderWindow window(VideoMode(J*MAP_SCALE, I*MAP_SCALE), "Platformer");

    Hero hero;
    RectangleShape rect (Vector2f(MAP_SCALE, MAP_SCALE));
    rect.setFillColor(Color::Black);

    CircleShape coin (COIN_SIZE);
    coin.setFillColor(Color::Yellow);

    Font font;
    font.loadFromFile("font.ttf");


//    for (int i=0; i<I; i++)
//                for (int j; j<J; j++)
//                    if (s_map[i][j] == '2') coin_count++;

    while (window.isOpen())
    {
        if (game_over)
        {
            hero.restart();
            s_map = maps[level_count];
            game_over = false;
        }
        if (win)
        {
            level_count++;
            if (level_count == level_amount)
                level_count = 2;
            hero.restart();
            s_map = maps[level_count];
            coin_count = 1;
            win = false;

        }
        Event event;
        while (window.pollEvent(event))
        {
            if (game_over || event.type==Event::Closed)
                window.close();
        }
        if (Keyboard::isKeyPressed(Keyboard::Left))
        {
            hero.dx = -SPEED_X;
        }
        if (Keyboard::isKeyPressed(Keyboard::Right))
        {
            hero.dx = SPEED_X;
        }
        if (Keyboard::isKeyPressed(Keyboard::Up))
        {
            if (!hero.in_air)
                hero.dy -= SPEED_Y;
        }
        if (Keyboard::isKeyPressed(Keyboard::Q))
        {
            win = true;
            sleep(milliseconds(100));
        }

        hero.update (coin_count);
        window.clear(Color::White);

        coin_count = 0;
        for (int i = 0; i < I; i++)
        {
            for (int j = 0; j<J; j++)
            {
                rect.setPosition(j*MAP_SCALE, i*MAP_SCALE);
                if (s_map[i][j] == '1')
                {
                    rect.setFillColor(Color::Black);
                    window.draw(rect);
                }
                if (s_map[i][j] == '2')
                {
                    coin_count++;
                    Color _color(255,104,0x00, 0xFF);
                    coin.setRadius(COIN_SIZE+2);
                    coin.setFillColor(_color);
                    coin.setPosition(Vector2f(j*MAP_SCALE + MAP_SCALE/2 - COIN_SIZE-2
                                              , i*MAP_SCALE + MAP_SCALE/2 - COIN_SIZE-2));
                    window.draw(coin);
                    coin.setRadius(COIN_SIZE);
                    _color.r = 0xFF;
                    _color.g = 0xFF;
                    _color.b = 0;
                    _color.a = 0xFF;
                    coin.setFillColor(_color);
                    coin.setPosition(Vector2f(j*MAP_SCALE + MAP_SCALE/2 - COIN_SIZE
                                              , i*MAP_SCALE + MAP_SCALE/2 - COIN_SIZE));
                    window.draw(coin);
                }
                if (s_map[i][j] == '3')
                {
                    rect.setFillColor(Color::Blue);
                    window.draw(rect);
                }
                if (s_map[i][j] == '4')
                {
                    rect.setFillColor(Color::Black);
                    window.draw(rect);
                }
                if (s_map[i][j] == '5')
                {
                    Color _color(255,104,0x00, 0xFF);
                    coin.setRadius(COIN_SIZE+2);
                    coin.setFillColor(_color);
                    coin.setPosition(Vector2f(j*MAP_SCALE + MAP_SCALE/2 - COIN_SIZE-2
                                              , i*MAP_SCALE + MAP_SCALE/2 - COIN_SIZE-2));
                    window.draw(coin);
                    coin.setRadius(COIN_SIZE);
                    _color.r = 0xFF;
                    _color.g = 0xFF;
                    _color.b = 0;
                    _color.a = 0xFF;
                    coin.setFillColor(_color);
                    coin.setPosition(Vector2f(j*MAP_SCALE + MAP_SCALE/2 - COIN_SIZE
                                              , i*MAP_SCALE + MAP_SCALE/2 - COIN_SIZE));
                    window.draw(coin);
                }
            }
        }

        std::stringstream str;
        if (coin_count!=0)
            str << "Coins :" << coin_count << "\nLevel: " << level_count+1 << "/" << level_amount;
        else
            str<< "You need\na miracle";
        Text text(str.str(), font, 25);
        text.setPosition(J*MAP_SCALE-150, 50);
        text.setColor(Color::Magenta);
        window.draw(text);

        window.draw(hero.circle);
        window.display();
    }
    return 0;
}
